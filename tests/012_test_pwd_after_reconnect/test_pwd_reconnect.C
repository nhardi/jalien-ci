int test_pwd_reconnect() {
    TGrid::Connect("alien://");
    gGrid->Mkdir("test");
    gGrid->Cd("test");
    TGrid::Connect("alien://");
    TString pwd = gGrid->Pwd();
    TString home = gGrid->GetHomeDirectory();

    if (pwd == home + "test/") {
        return 0;
    } else {
        return 1;
    }
}
