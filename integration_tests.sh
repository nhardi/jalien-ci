#!/bin/bash
export ROOT="$PWD"
export CVMFS_PREFIX="/cvmfs/alice.cern.ch/bin"
. $ROOT/utils/discovery.sh
export TESTS=$ROOT/tests

[ "$1" == "--keep-going" ] && export KEEP_GOING="True" && shift
run_test_suite $@
